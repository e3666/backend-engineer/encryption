# Sumário

- [**Encryption**](#encryption)
- [**Symmetric**](#symmetric)
    - [Prós](#prós)
    - [Contras](#contras)
- [**Asymmetric**](#asymmetric)
    - [Prós](#prós)
    - [Contras](#contras)
- [**Como contornar os problemas**](#como-contornar-os-problemas)
- [**Referência**](#referência)


# Encryption

Criptografar algo consiste em pegar uma informação, percorrê-la efetuando modificações conforme uma chave, para que ninguém que não tenha essa chave seja capaz de ler essa informação.

A problemática começa quando precisamos transitar informações pela internet de forma criptografada, pois ambas as partes da comunicação devem possuir a mesma chave. Para isso temos duas formas, a simétrica e assimétrica.

# Symmetric

A forma simétrica propõe que ao criar uma chave para criptografia, a pessoa A à envie para a pessoa B, para que ela possa descriptografar a informação enviada. Porém uma vez que essa chave transita entre essas duas pessoas, ela pode ser interceptada, e uma terceira pessoa passa a conseguir descriptografar tudo que elas transitam de forma criptografada.

## Prós

- O processo de criptografar e descriptografar é rápido
- Por ser rápido, é eficiente para arquivos grandes

## Contras

- Muito complicado transportar essa chave de forma segura

# Asymmetric

A ideia aqui é termos duas chaves, uma privada e uma pública. Sendo que entre elas existe um jogo de números primos, sendo eles extremamente grandes. E como elas se completam, uma pessoa que possui somente a chave pública não consegue fazer nada além de criptografar algo, mas nunca descriptografar. Já a chave privada é projetada para quebrar essa criptografia da chave pública facilmente, sendo que sem ela um computador demoraria anos, décadas, dependendo até centenas de anos, tentando calcular na força bruta.

- Sendo assim o fluxo funciona da seguinte forma:
    - A pessoa A gera um par de chaves
    - Envia a pública para B
    - A pessoa B criptografa a informação que enviará para A com a chave pública de A
    - Assim que a mensagem de B chegar em A, A utiliza sua chave privada e descriptografa a mensagem de B

Essa ideia é o RSA, que está presente no SSH.

> OBS.: Veja os vídeos da referência para entender melhor como funciona o processo matemático das chaves (de Eddie Woo e Art of the Problem).

## Prós

- A chave publica pode ser compartilhada, logo é fácil compartilhar a chave com segurança

## Contras

- O processo de criptografar e descriptografar é lento
- Por ser lento, ele é ruim para arquivos grandes

# Como contornar os problemas

Bom, se levarmos em conta que o processo assimétrico é seguro para compartilhar a chave, e bom para enviar poucos dados, podemos criptografar a chave simétrica, enviá-la de forma assimétrica e depois estabelecemos a comunicação simétrica. Essa forma de trabalhar é o TSL.

# Referência

- [Symmetrical vs asymmetrical Encryption Pros and Cons by Example - Hussein Nasser](https://www.youtube.com/watch?v=Z3FwixsBE94&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=8)
- [The RSA Encryption Algorithm (1 of 2: Computing an Example) - Eddie Woo](https://www.youtube.com/watch?v=4zahvcJ9glg)
- [The RSA Encryption Algorithm (2 of 2: Generating the Keys) - Eddie Woo](https://www.youtube.com/watch?v=oOcTVTpUsPQ)
- [Public Key Cryptography: RSA Encryption Algorithm - Art of the Problem](https://www.youtube.com/watch?v=wXB-V_Keiu8)